output service_account_name {
  value = aws_iam_user.service_account.name
}

output aws_access_key_id {
  value     = aws_iam_access_key.service_account.id
  sensitive = true
}

output aws_secret_access_key {
  value     = aws_iam_access_key.service_account.secret
  sensitive = true
}
