resource aws_iam_user service_account {
  name = local.name
  path = local.iam_path

  tags = local.tags
}

resource aws_iam_access_key service_account {
  user = aws_iam_user.service_account.name
}

resource aws_iam_group service_account {
  name = local.name
  path = local.iam_path
}

resource aws_iam_group_membership service_account {
  name = "service-account"
  users = [
    aws_iam_user.service_account.name
  ]

  group = aws_iam_group.service_account.name
}
