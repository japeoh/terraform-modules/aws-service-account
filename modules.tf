module meta {
  source = "git::https://gitlab.com/ricfeatherstone1/tfmodules/meta.git?ref=v0.1"

  domain      = var.domain
  unit        = var.unit
  environment = var.environment
  project     = var.project
}
