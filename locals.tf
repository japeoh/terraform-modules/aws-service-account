locals {
  name     = format("%s.%s", var.name, module.meta.name_suffix)
  iam_path = format("/system/%s/%s/%s/", var.unit, var.environment, var.project)

  tags = merge(module.meta.tags,
    map(
      "Name", local.name,
      "Type", "service-account"
    )
  )
}
